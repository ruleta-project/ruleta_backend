package com.ruletaApp.Backend.Repository;

import com.ruletaApp.Backend.Domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> { 

  
    Users findByUsername(String username);




}
