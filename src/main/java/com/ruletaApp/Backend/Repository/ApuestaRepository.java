package com.ruletaApp.Backend.Repository;

import com.ruletaApp.Backend.Domain.Apuestas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApuestaRepository extends JpaRepository<Apuestas, Long> {
    
    Apuestas findByDesc(String desc);

}
