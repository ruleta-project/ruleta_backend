package com.ruletaApp.Backend;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;


@SpringBootApplication
public class ruletaAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ruletaAppApplication.class, args);
	}

}

// package com.ruletaApp.Backend;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.SpringApplication;
// import org.springframework.boot.CommandLineRunner;
// import org.springframework.boot.autoconfigure.SpringBootApplication;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

// @SpringBootApplication
// public class ruletaAppApplication implements CommandLineRunner {

// 	@Autowired
// 	private BCryptPasswordEncoder passwordEncoder;

// 	public static void main(String[] args) {
// 		SpringApplication.run(ruletaAppApplication.class, args);
// 	}

// 	@Override
// 	public void run(String... args) throws Exception {

// 		String password = "1234";
// 		for (int i = 0; i < 1; i++) {
// 			String passwordBCry = passwordEncoder.encode(password);
// 			System.out.println(passwordBCry);

// 		}

// 	}
// }
