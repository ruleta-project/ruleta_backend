package com.ruletaApp.Backend.Service.transformer;

import com.ruletaApp.Backend.Domain.Users;
import com.ruletaApp.Backend.Service.dto.AccountDTO;

public class AccountTransformer {

    public static AccountDTO getAccountDTOFromUser(Users user) {
        if (user == null) {
            return null;
        }
        AccountDTO dto = new AccountDTO();

        dto.setId(user.getId());
        dto.setNombre(user.getNombre());
        dto.setUsername(user.getUsername());
        dto.setSaldo(user.getSaldo());
        dto.setPassword(user.getPassword());
        dto.setEstado(user.getEstado());
        dto.setFechaCreacion(user.getFechaCreacion());
        dto.setRolsUsers(user.getRolsUsers());
        return dto;

    }
}
