package com.ruletaApp.Backend.Service.transformer;

import com.ruletaApp.Backend.Domain.Apuestas;
import com.ruletaApp.Backend.Service.dto.ApuestasDTO;

public class ApuestaTransformer {
    public static ApuestasDTO getApuestaDTOFromApuesta(Apuestas apuesta) {

        if (apuesta == null) {
            return null;
        }

        ApuestasDTO dto = new ApuestasDTO();
        /**
         * setting of variables
         */
        dto.setId(apuesta.getId());
        dto.setDesc(apuesta.getDesc());
        dto.setUser(apuesta.getUser());

        return dto;

    }

    public static Apuestas getapuestasFromapuestasDTO(ApuestasDTO dto) {

        if (dto == null) {
            return null;
        }
        Apuestas apuesta = new Apuestas();
        /**
         * setting of variables
         */
        apuesta.setId(dto.getId());
        apuesta.setDesc(dto.getDesc());
        apuesta.setUser(dto.getUser());

        return apuesta;
    }
}
