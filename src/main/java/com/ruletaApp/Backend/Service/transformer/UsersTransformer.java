package com.ruletaApp.Backend.Service.transformer;

import com.ruletaApp.Backend.Domain.Users;
import com.ruletaApp.Backend.Service.dto.UsersDTO;

public class UsersTransformer {
    public static UsersDTO getUsersDTOFromUsers(Users user) {

        if (user == null) {
            return null;
        }
        UsersDTO dto = new UsersDTO();
        /**
         * setting of variables
         */
        dto.setId(user.getId());
        dto.setNombre(user.getNombre());
        dto.setUsername(user.getUsername());
        dto.setSaldo(user.getSaldo());
        dto.setPassword(user.getPassword());
        dto.setEstado(user.getEstado());
        dto.setFechaCreacion(user.getFechaCreacion());
        dto.setRolsUsers(user.getRolsUsers());

        return dto;
    }

    public static Users getUsersFromUsersDTO(UsersDTO dto) {

        if (dto == null) {
            return null;
        }
        Users user = new Users();
        /**
         * setting of variables
         */
        user.setId(dto.getId());
        user.setNombre(dto.getNombre());
        user.setUsername(dto.getUsername());
        user.setSaldo(dto.getSaldo());
        user.setPassword(dto.getPassword());
        user.setEstado(dto.getEstado());
        user.setFechaCreacion(dto.getFechaCreacion());
        user.setRolsUsers(dto.getRolsUsers());
        return user;
    }
 
}
