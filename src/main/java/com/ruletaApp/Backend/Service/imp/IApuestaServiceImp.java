package com.ruletaApp.Backend.Service.imp;

import java.time.LocalDate;

import com.ruletaApp.Backend.Domain.Apuestas;
import com.ruletaApp.Backend.Repository.ApuestaRepository;
import com.ruletaApp.Backend.Service.IApuestaService;
import com.ruletaApp.Backend.Service.dto.ApuestasDTO;
import com.ruletaApp.Backend.Service.error.ObjectNotFoundException;
import com.ruletaApp.Backend.Service.transformer.ApuestaTransformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class IApuestaServiceImp implements IApuestaService{


    @Autowired
    ApuestaRepository apuestasRepository;


    /**
     * this service allows the creation of apuesta details
     * 
     * @param apuestaDTO
     * @return
     */
    @Override
    public ApuestasDTO create(ApuestasDTO apuestaDTO) {
        Apuestas apuesta = ApuestaTransformer.getapuestasFromapuestasDTO(apuestaDTO);
        return ApuestaTransformer.getApuestaDTOFromApuesta(apuestasRepository.save(apuesta));
    }

    /**
     * this service allows to bring by page the data created from apuesta details
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @Override
    public Page<ApuestasDTO> read(Integer pageSize, Integer pageNumber) {

        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        return apuestasRepository.findAll(pageable).map(ApuestaTransformer::getApuestaDTOFromApuesta);
    }

    /**
     * this service allows you to update the data created in apuesta details
     * 
     * @param ApuestaDTO
     * @return
     */
    @Override
    public ApuestasDTO update(ApuestasDTO apuestaDTO) {
        Apuestas apuesta = ApuestaTransformer.getapuestasFromapuestasDTO(apuestaDTO);
        return (ApuestaTransformer.getApuestaDTOFromApuesta(apuestasRepository.save(apuesta)));
    }

    /***
     * Implement delete service
     * 
     * @param id
     * @return
     */
    @Override
    public String deleteById(Long id) {

        apuestasRepository.deleteById(id);
        return "has been deleted";
    }

    /**
     * this service allows you to get the apuesta details by id
     * 
     * @param id
     * @return
     */
    @Override
    public ApuestasDTO getById(Long id) {
        Optional<Apuestas> apuesta = apuestasRepository.findById(id);
        if (!apuesta.isPresent()) {
            throw new ObjectNotFoundException("Error: La apuesta con el id = " + id + " no existe");
        }
        return apuesta.map(ApuestaTransformer::getApuestaDTOFromApuesta).get();
    } 
    
}
