package com.ruletaApp.Backend.Service.imp;

import java.time.LocalDate;
import com.ruletaApp.Backend.Domain.Users;
import com.ruletaApp.Backend.Repository.UsersRepository;
import com.ruletaApp.Backend.Service.IUsersService;
import com.ruletaApp.Backend.Service.dto.AccountDTO;
import com.ruletaApp.Backend.Service.dto.UsersDTO;
import com.ruletaApp.Backend.Service.transformer.AccountTransformer;
import com.ruletaApp.Backend.Service.transformer.UsersTransformer;
import com.ruletaApp.Backend.Service.error.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class IUsersServiceImp implements IUsersService{

    private final Logger logger = LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    /**
     * this service allows the creation of user details
     * 
     * @param usersDTO
     * @return
     */
    @Override
    public UsersDTO create(UsersDTO usersDTO) {

        usersDTO.setSaldo(15000.00);
        usersDTO.setEstado(true);
        usersDTO.setPassword(passwordEncoder.encode(usersDTO.getPassword()));
        usersDTO.setFechaCreacion(LocalDate.now());
        Users user = UsersTransformer.getUsersFromUsersDTO(usersDTO);

        return UsersTransformer
                .getUsersDTOFromUsers(usersRepository.save(user));
    }
  

    /**
     * this service allows to bring by page the data created from user details
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @Override
    public Page<UsersDTO> read(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return usersRepository.findAll(pageable).map(UsersTransformer::getUsersDTOFromUsers);
    }

    /**
     * this service allows you to update the data created in user details
     * 
     * @param UsersDTO
     * @return
     */
    @Override
    public UsersDTO update(UsersDTO UsersDTO) {
        Users user = UsersTransformer.getUsersFromUsersDTO(UsersDTO);
        return (UsersTransformer.getUsersDTOFromUsers(usersRepository.save(user)));
    }

    /***
     * Implement delete service
     * 
     * @param id
     * @return
     */
    @Override
    public String deleteById(Long id) {

        usersRepository.deleteById(id);
        return "has been deleted";
    }

    /**
     * this service allows you to get the user details by id
     * 
     * @param id
     * @return
     */
    @Override
    public UsersDTO getById(Long id) {
        Optional<Users> user = usersRepository.findById(id);
        if (!user.isPresent()) {
            throw new ObjectNotFoundException("Error: El detalle de usuario con el id = " + id + " no existe");
        }
        return user.map(UsersTransformer::getUsersDTOFromUsers).get();
    }

    @Override
    public AccountDTO getAccountUser(String username) throws UsernameNotFoundException {
        Users user = usersRepository.findByUsername(username);

        if (username == null) {
            logger.error("Error el usuario " + username + " NO se encuentra");
            throw new UsernameNotFoundException("Error el usuario " + username + " NO se encuentra");
        }
        AccountDTO dto = AccountTransformer.getAccountDTOFromUser(user);
        ArrayList rols = new ArrayList<String>();

        dto.getRolsUsers().forEach(item -> {
            rols.add(item.getRol());
        });
        /* SET AUTHORITIES */
        dto.setAuthorities(rols);
        return dto;
    }
}
