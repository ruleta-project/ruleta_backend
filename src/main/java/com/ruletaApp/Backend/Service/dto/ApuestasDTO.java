package com.ruletaApp.Backend.Service.dto;

import javax.persistence.ManyToOne;
import com.ruletaApp.Backend.Domain.Users;
import com.ruletaApp.Backend.Domain.enums.Color;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApuestasDTO {

    private Long id;

    private Color desc;

    @ManyToOne
    private Users user;
}
