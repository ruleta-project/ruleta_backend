package com.ruletaApp.Backend.Service.dto;

import com.ruletaApp.Backend.Domain.RolsUser;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AccountDTO implements Serializable{

    private Long id;
    private String nombre;
    private String username;
    private Double saldo;
    private String password;
    private Boolean estado;
    private LocalDate fechaCreacion;
    private List<RolsUser> rolsUsers;
    private ArrayList authorities;
    
}
