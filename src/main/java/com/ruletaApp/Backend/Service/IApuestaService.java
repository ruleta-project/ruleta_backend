package com.ruletaApp.Backend.Service;

import com.ruletaApp.Backend.Service.dto.ApuestasDTO;
import org.springframework.data.domain.Page;

public interface IApuestaService {


    public ApuestasDTO create(ApuestasDTO tasksDTO);

    public Page<ApuestasDTO> read(Integer pageSize, Integer pageNumber);
    

    public ApuestasDTO update(ApuestasDTO tasksDTO);

    /**
     * this service allows you to get the user details by id
     * 
     * @param id
     * @return
     */
    public ApuestasDTO getById(Long id);

    /**
     * Implement delete service
     * 
     * @param id
     * @return
     */
    public String deleteById(Long id);

    
}
