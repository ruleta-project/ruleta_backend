package com.ruletaApp.Backend.Domain;

import lombok.*;
import javax.persistence.*;
import com.ruletaApp.Backend.Domain.enums.Color;
import java.io.Serializable;

@Getter
@Setter
@Entity
public class Apuestas implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "document_type", nullable = false)
    private Color desc;


    @ManyToOne
    private Users user;



}
