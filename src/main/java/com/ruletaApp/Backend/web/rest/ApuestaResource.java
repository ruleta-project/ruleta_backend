package com.ruletaApp.Backend.web.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;

import com.ruletaApp.Backend.Repository.ApuestaRepository;
import com.ruletaApp.Backend.Service.IApuestaService;
import com.ruletaApp.Backend.Service.dto.ApuestasDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
@Api(value = "Web service apuesta", description = "Web service for CRUD apuesta")
public class ApuestaResource {


    @Autowired
    IApuestaService apuestaService;

    @Autowired
    ApuestaRepository apuestaRepository;
    

    /**
     * This method allows you to list apuestas by page
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @GetMapping("/apuestas")
    @ApiOperation(value = "List apuesta", notes = "Return a apuesta Listed")
    public Page<ApuestasDTO> read(@PathParam("pageSize") Integer pageSize, @PathParam("pageNumber") Integer pageNumber) {
        return apuestaService.read(pageSize, pageNumber);
    }


    /**
     * this method allows you to get tapuesta
     * 
     * @param id
     * @return
     */
    @GetMapping("/apuesta/{id}")
    public ResponseEntity<ApuestasDTO> getById(@PathVariable Long id) {
        return new ResponseEntity<>(apuestaService.getById(id), HttpStatus.OK);
    }

    
   /***
     *This method allows you to create apuesta
     * @param ApuestasDTO
     * @return
     */
    @PostMapping("/apuesta")
    @ApiOperation(value = "Create apuesta", notes = "Return a apuesta created")
    public ResponseEntity<?> create(@RequestBody ApuestasDTO apuestasDTO, BindingResult result){
        ApuestasDTO dto = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e -> "Error en el campo " + e.getField() + ": " + e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try{
            dto = apuestaService.create(apuestasDTO);
        } catch (DataAccessException err) {
            response.put("message", "Error al crear una apuesta en la base de datos");
            response.put("error", err.getMessage() + ": " +
                    err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "La apuesta ha sido creada correctamente");
        response.put("Tarea ", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /***
     * this method allows you to update the data created in apuesta
     * 
     * @param apuesta
     * @return
     */
    @PutMapping("/apuesta")
    public ApuestasDTO update(@RequestBody ApuestasDTO apuestasDTO) {
        return apuestaService.update(apuestasDTO);
    }

    /**
     * Implement delete method
     * 
     * @param id;
     */
    @DeleteMapping("/apuesta/{id}")
    public void deleteapuesta(@PathVariable Long id) {
        apuestaRepository.deleteById(id);
    }
    
}
