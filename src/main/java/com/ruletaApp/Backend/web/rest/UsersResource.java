package com.ruletaApp.Backend.web.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;
import com.ruletaApp.Backend.Repository.UsersRepository;
import com.ruletaApp.Backend.Service.IUsersService;
import com.ruletaApp.Backend.Service.dto.AccountDTO;
import com.ruletaApp.Backend.Service.dto.UsersDTO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
@Api(value = "Web service user", description = "Web service for CRUD user")
public class UsersResource {

    @Autowired
    IUsersService userService;

    @Autowired
    UsersRepository userRepository;

    /**
     * This method allows you to list users by page
     * 
     * @param pageSize
     * @param pageNumber
     * @return
     */
    @GetMapping("/users")
    @ApiOperation(value = "List user", notes = "Return a user Listed")
    public Page<UsersDTO> read(@PathParam("pageSize") Integer pageSize, @PathParam("pageNumber") Integer pageNumber) {
        return userService.read(pageSize, pageNumber);
    }

    /**
     * API user/account , get information personal of users account
     * 
     * @param email
     * @return
     */
    @GetMapping("/user/account")
    public ResponseEntity<AccountDTO> getAccount(@RequestParam(value = "username") String username) {
        return new ResponseEntity<>(userService.getAccountUser(username), HttpStatus.OK);
    }

    /**
     * this method allows you to get the user details by id
     * 
     * @param id
     * @return
     */
    @GetMapping("/user/{id}")
    public ResponseEntity<UsersDTO> getById(@PathVariable Long id) {
        return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
    }

    
   /***
     *This method allows you to create users
     * @param usersDTO
     * @return
     */
    @PostMapping("/user")
    @ApiOperation(value = "Create user", notes = "Return a user created")
    public ResponseEntity<?> create(@RequestBody UsersDTO usersDTO, BindingResult result){
        UsersDTO dto = null;
        Map<String, Object> response = new HashMap<>();

        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(e -> "Error en el campo " + e.getField() + ": " + e.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("error", errors);
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try{
            dto = userService.create(usersDTO);
        } catch (DataAccessException err) {
            response.put("message", "Error al crear un usuario en la base de datos");
            response.put("error", err.getMessage() + ": " +
                    err.getMostSpecificCause().getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.put("message", "El usuario ha sido creada correctamente");
        response.put("Usuario ", dto);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /***
     * this method allows you to update the data created in user details
     * 
     * @param detailUserDTO
     * @return
     */
    @PutMapping("/user")
    public UsersDTO update(@RequestBody UsersDTO usersDTO) {
        return userService.update(usersDTO);
    }

    /**
     * Implement delete method
     * 
     * @param id;
     */
    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        userRepository.deleteById(id);
    }
    
}
