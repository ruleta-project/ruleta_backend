## Ruleta Backend
**Pasos para empezar a funcionar el backend**

Preferiblemente se recomienda correr el proyecto en un sistema operativo linux.

1. Tener las siguientes instalaciones:
- Tener java version 1.8.
`sudo apt install openjdk-8-jdk`
- Tener instalado maven
`sudo apt install maven`
- Instalar pgAdmin y postgresql
`sudo apt install postgresql postgresql-contrib`


2. Crear la base de datos 
- entrar a la consola de psql `sudo -i -u postgres psql`
- crear la base de datos `CREATE DATABASE ruleta`

**Nota importante*** la base de datos debe llamarse 'ruleta'

3. Clonar el proyecto e instalar dependencias dentro del directorio donde se enceuntre el archivo pom.xml
`mvn install`

4. Por ultimo correr el servidor con el siguiente comando
`mvn spring-boot:run` o simplemente ejecutar el archivo **ruletaAppApplication.java**
